FROM python:3.7-slim

WORKDIR /code

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY ./app_data ./app_data

COPY ./main_app ./main_app

COPY server.py .

CMD [ "python3", "./server.py" ]
