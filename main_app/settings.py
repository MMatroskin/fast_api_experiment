from pathlib import Path


ROOT_DIR = Path(__file__).resolve().parent.parent
DATA_DIR = r'app_data'
CONFIG = r'app.ini'
