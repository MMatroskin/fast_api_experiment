from os.path import join
import configparser
from collections import namedtuple
from main_app.settings import ROOT_DIR, DATA_DIR, CONFIG


Result = namedtuple('Result', 'success, message')


def get_config(filename=CONFIG):
    config_path = join(ROOT_DIR, DATA_DIR, filename)
    config = configparser.ConfigParser()
    config.read(config_path)
    return config
