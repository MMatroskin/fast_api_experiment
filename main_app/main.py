from fastapi import FastAPI, Request
from bson import ObjectId
from main_app.db_srv import db
from main_app.models import Employee
from main_app.common import get_config


config = get_config()
collection = config.get('main', 'collection')

app = FastAPI()


@app.get('/')
async def index():
    return {'message': 'Hello! This is simple FastAPI app.'}


@app.get('/employees')
async def get_items(request: Request):
    """
    Returns employees filtered by some params
    :param request: filters and filters values
    :return: json with list of employees
    """
    params = request.query_params
    query_item = dict(params)
    result = [Employee(**item) for item in db[collection].find(query_item)]
    return {'data': result}


@app.get('/employees/{item_id}')
async def get_item_by_id(item_id: str):
    """
    Returns one of employees
    :param item_id: id of a employee
    :return: json with a employee
    """
    item = db[collection].find_one({'_id': ObjectId(item_id)})
    return {'data': Employee(**item)}
