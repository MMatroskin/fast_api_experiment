from pymongo import MongoClient
from main_app.common import get_config


config = get_config()
db_host = config.get('main', 'db_host')
db_port = config.getint('main', 'db_port')
schema = config.get('main', 'schema')

client = MongoClient(db_host, db_port)
db = client[schema]
