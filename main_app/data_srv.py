import json
from os.path import join
from main_app.common import get_config, Result
from main_app.db_srv import db
from main_app.settings import ROOT_DIR, DATA_DIR


def main():
    config = get_config()
    data_src = config.get('main', 'data_src')
    collection = config.get('main', 'collection')
    res = insert_data(join(ROOT_DIR, DATA_DIR, data_src), collection)
    print('Done' if res.success else res.message)


def insert_data(file_name: str, collection_name: str):
    success = True
    message = None
    with open(file_name, encoding='utf-8') as f:
        data = json.load(f)
    collection = db[collection_name]
    try:
        collection.insert_many(data)
    except Exception as ex:
        success = False
        message = str(ex)
    return Result(success, message)


if __name__ == '__main__':
    main()
