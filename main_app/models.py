from pydantic import BaseModel
import datetime


class Employee(BaseModel):
    name: str
    age: int
    email: str
    company: str
    join_date: datetime.datetime
    job_title: str
    gender: str
    salary: int
