Simple project with FastApi, MongoDB, Docker

- fill database: python main_app/data_srv.py

- start app: python server.py

- query example: http://127.0.0.1:8000/employees?company=Yandex&gender=other

- database Ip bind: 0.0.0.0

- start container: docker run -d --name some_container -p 127.0.0.1:9000:8000 some_img
